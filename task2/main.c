//Credit for list of words goes to Xethron on github,
//I just took out some words (mostly two letter ones)
//link to Xethron's list of hangman words:
//  https://github.com/Xethron/Hangman/blob/master/words.txt
//I also found out about isalnum() here:
//  https://www.tutorialspoint.com/isalnum-function-in-c-language


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include<time.h>
#include<ctype.h>   //this library contains isalnum() function


#define MAX_WORD_LENGTH 20  //no word should be longer than 20
#define NUMBER_OF_WORDS 835 //ammount of words, change accordingly, when changing words.txt

void load_words(char words[][MAX_WORD_LENGTH],int* size);
int length_of_word(char words[]);
int print_word(char array[],char printed[],int word_length,int counter,char already_inputed[],char input);
void already_tried(char already_inputed[]);
int fail_counter(char array[],char already_inputed[],int word_length);
void draw_hangman(int fails);


int main(){
    printf("\nInput lowercase english alphabet characters. Input multiple at your own peril!\n");
    //initialized variables, to hold words
    char words [NUMBER_OF_WORDS][MAX_WORD_LENGTH];
    int size;
    load_words(words,&size);

    //use time to get a random seed
    srand(time(NULL));
    int rand_num = (rand()%NUMBER_OF_WORDS);
    //get a random number

    int word_length = length_of_word(words[rand_num]);
    //calculate the length of the randomly chosen word
    //then use it as length for the array of characters
    char array[word_length];

    //input the randomly chosen word into the array of characters
    for (int i = 0; i < word_length; i++)
    {
        array[i] = words[rand_num][i];
    }

    char printed[word_length];
    //the array, that will be printed
    //it is first filled with '_' and printed out for the first time
    for (int i = 0; i < word_length; i++)
    {   
        printed[i] = '_';
        printf("_\t");
    }   

    //array, that holds already tried inputs
    //it gets filled with '?' because no word will use this symbol
    char already_inputed[50];
    for (int i = 0; i < 50; i++)
    {
        already_inputed[i] = '?';
    }

    //variables, that dictate the end of the game
    int guessed_characters_counter = 0;
    int fails = 0;
    int end_of_game = 1;
    int end_state = 0;

    while (end_of_game)
    {
        //first scan user input
        char inputed_character;
        scanf(" %c",&inputed_character);

        //this is a long boy :)
        guessed_characters_counter = print_word(array,printed,word_length,guessed_characters_counter,already_inputed,inputed_character);

        already_tried(already_inputed);
        fails=fail_counter(array,already_inputed,word_length);
        draw_hangman(fails);

        //if player failed more than 7 times end game with state 0 (defeat)
        if (fails > 7)
        {
            end_of_game =0 ;
            end_state = 0;
        }
        //if player guessed all characters, end game with state 1 (victory)
        if (guessed_characters_counter == word_length)
        {
            end_of_game = 0;
            end_state = 1;
        }
    }
    //check whether the user won or lost and print the correct message
    if (end_state == 1)
    {
        printf("Congrats, you win!\nThe word was %s\n",words[rand_num]);
    }else
    {
        printf("You've been hanged!\nThe word was %s\n",words[rand_num]);
    }
}

void draw_hangman(int fails){
    //draw lines according to number of fails
    printf("\n");
    if (fails > 7)
    {
        printf(" ________\n");
    }
    if (fails > 6)
    {
        printf(" |/     |\n");
    }
    if (fails > 5)
    {
        printf(" |      O\n");
    }
    if (fails > 4)
    {
        printf(" |     /|\\ \n");
    }
    if (fails > 3)
    {
        printf(" |     / \\ \n");  
    }
    if (fails > 2)
    {
        printf(" |\n");
    }
    if (fails > 1)
    {
        printf(" |\n");
    }
    if (fails > 0)
    {
        printf("/ \\\n");
    }
    
}


int fail_counter(char array[],char already_inputed[],int word_length){
    int succes = 0;
    int tries = 0;
    for (int i = 0; i < 50; i++)
    {
        //it goes through the already_inputed array
        if (already_inputed[i]=='?')
        {
            //if the value is '?', then we passed the inputed values,
            //there is no need to check further
            break;
        }else
        {
            //in the opposite scenario, check if the character is part of the word
            for (int j = 0; j < word_length; j++)
            {
                //if it is, then add 1 to succes variable and try the next character
                //(breaking this loop avoids counting the same symbols as multiple successes)
                if (already_inputed[i]==array[j])
                {
                    succes++;
                    break;
                }
            }
            //the amount of tries(basically the number of non '?' characters in already_inputed)
            tries++;
        }
    }
        //return number of fails
        return (tries-succes);
}


void already_tried(char already_inputed[]){
    //print all of the already tried characters
    printf("\nAlready tried characters:");
    for (int i = 0; i < 50; i++)
    {
        if (already_inputed[i]!='?')
        {
            printf("%c,",already_inputed[i]);
        }
    }
    
}

int print_word(char array[],char printed[],int word_length,int counter,char already_inputed[],char input){
    
    //first check if input was already tried before
    for (int i = 0; i < 50; i++)
    {
        if (input == already_inputed[i])
        {
            //if character was already tried warn the user and exit the function
            printf("\nYou already tried this character!");
            return counter;
        }else if (already_inputed[i] == '?')
        {
            //if already_inputed is '?' it means, that we went through all of the already tried 
            //inputs, thus we can continue
            already_inputed[i] = input;
            break;
        }  
    }

    printf("\n");
    //check if input is one or more letters of the word
    //if so, write that character in the printed array 
    //and add 1 to the counter of guessed letters
    for (int i = 0; i < word_length; i++)
    {   
        if (input == array[i])
        {
            printed[i] = input;
            counter+=1;
        }
    printf("%c\t",printed[i]);
    }

    //return counter which will be written inside the guessed_characters_counter
    return counter;
}


int length_of_word(char words[]){
    //the length of the words is not actualy MAX_WORD_LENGTH
    //thus we need to find the actual length
    int counter = 0;
    for (int i = 0; i < MAX_WORD_LENGTH; i++)
    {
        //go throught the random word and if the character on index i
        //is alphanumerical, augment counter by 1
        if (isalnum( words[i]))
        {
            counter+=1;
        }
    }  
    //the returned value will be used as length for the charr array[]
    return counter;
}

void load_words(char words[][MAX_WORD_LENGTH],int* size){
    //some alphanumeric characters tend to linger inside the RAM
    //so I purge them, by changing every character inside this array
    //with '!'(since it is not alphanumeric)
    for (int i = 0; i < NUMBER_OF_WORDS; i++)
    {
        for (int j = 0; j < MAX_WORD_LENGTH; j++)
        {
            words[i][j]='!';
        }
        
    }
    
    //open the file with intent of reading it
    FILE* file = fopen("words.txt", "r");

    if (file == NULL)
    {
        //if open didn't go well, warn the user and stop the function
        printf("Error: Can't open file for reading!\n");
        return;
    }

    char line[MAX_WORD_LENGTH];
    for (int i = 0; i < MAX_WORD_LENGTH; i++)
    {
        line[i]='?';
    }
    
    *size = 0;
    //copy the contents of the file inside the words array
    while (fgets(line, sizeof(line), file) != NULL)
    {
        strcpy(words[*size],line);
        *size+=1;
    }

    //close the file
    fclose(file);
    
}
