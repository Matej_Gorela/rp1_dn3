#include <stdio.h>

char input_validation (char user_input, char possible_inputs[]);
char floor_change(char user_input, char current_floor, char possible_inputs[]);


int main (){

    char current_floor ='P';//when the program is started, the floor is P
    while (1)               //infinite while loop
    {    
        //informs the user on the current floor
        printf("You are currently on floor %c\n",current_floor);
        char possible_inputs[]={'P','1','2','3','4','5'};//list of valid inputs

        char user_input;
        scanf(" %c",&user_input);//scan for user input

        //validate user input, if validation returns 'e', the input was invalid
        if (input_validation(user_input,possible_inputs)=='e')
        {
            //if input is invalid, warn the user, and instruct him on valid inputs
            printf("Wrong input! Valid inputs are: 'P', '1', '2', '3', '4', '5'\n");
        }else
        {
            //if input was valid start the change_floor function and overwrite current_floor variable
            current_floor=floor_change(user_input,current_floor,possible_inputs);
        }
    }
}

char floor_change(char user_input, char current_floor, char possible_inputs[]){
    int current_floor_number=0;
    int desired_floor_number=0;
    
    for (int i = 0; i < sizeof(&possible_inputs); i++)
    {   //goes through the list of valid inputs, to find out which floor we are on
        //and where we want to go in numerical values
        if (possible_inputs[i]==user_input)
        {
            desired_floor_number=i;
        }
            
        if (possible_inputs[i]==current_floor)
        {
            current_floor_number=i;
        }
    }
        //checks whether the desire floor is equal, lower or higher than the current floor
    if (current_floor_number==desired_floor_number)
    {   
        //if the floor numbers are the same, warn the user
        printf("You're already on this floor you dingus!\n");
    }else if (current_floor_number > desired_floor_number)
    {   
        //if current floor is higher than desired floor, go down
        for (int a = current_floor_number; a >= desired_floor_number; a--)
        {
            //ask the user, if he wants to stop on each floor, on the way to the desired floor
            char answer;
            current_floor_number=a;
            current_floor=possible_inputs[current_floor_number];
            printf("You are currently on floor %c\n",current_floor);
            printf("Do you want to exit on this floor? Type Y or y for yes or anything else for no.\n");
            scanf(" %c",&answer);
            if ((answer=='Y')||(answer=='y'))
            {
                //if user wants to exit, before the desired floor, stop the for loop
                break;
            }       
        }
    }else
    {   //same code, just reversed, for going up
        for (int a = current_floor_number; a <= desired_floor_number; a++)
        {
            char answer;
            current_floor_number=a;
            current_floor=possible_inputs[current_floor_number];
            printf("You are currently on floor %c\n",current_floor);
            printf("Do you want to exit on this floor? Type Y or y for yes or anything else for no.\n");
            scanf(" %c",&answer);
            if ((answer=='Y')||(answer=='y'))
            {
                break;
            }                
        }
    }
    return current_floor;
}

char input_validation (char user_input,char possible_inputs[]){
    for (int i = 0; i < sizeof(&possible_inputs); i++)
    {
        if (user_input==possible_inputs[i]) //checks if user input is one of the possible inputs
        {
            return user_input;              //if yes, it returns the input
        }   
    } 
    return 'e';                             //if no it returns 'e'
}